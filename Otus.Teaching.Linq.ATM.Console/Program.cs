﻿using System.Linq;
using System.Threading;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");
            Thread.Sleep(1000);

            var atmManager = CreateATMManager();

            new ATMUI(atmManager).Run();
            
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
            Thread.Sleep(1000);
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}