﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User SignIn(string login, string password)
        {
            return Users
                .FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public void UserAccounts(int id)
        {
            var accounts = Accounts
                .Where(x => x.UserId == id)
                .Select(x => new
                {
                    openingDate = x.OpeningDate,
                    cashAll = x.CashAll
                })
                .ToList();

            foreach (var account in accounts)
            {
                Console.WriteLine($"Opening date: {account.openingDate} - cash: {account.cashAll}");
            }
        }

        public void UserHistory(int id)
        {
            var accounts = Accounts
                .Where(x => x.UserId == id)
                .Select(x => new
                {
                    openingDate = x.OpeningDate,
                    cashAll = x.CashAll,
                    history = History
                    .Where(y => y.AccountId == x.Id)
                    .Select(y => new
                    {
                        operationType = y.OperationType,
                        operationDate = y.OperationDate,
                        cashSum = y.CashSum
                    })
                    .ToList()
                })
                .ToList();

            foreach (var account in accounts)
            {
                Console.WriteLine($"Opening date: {account.openingDate} - cash: {account.cashAll}");

                foreach (var tx in account.history)
                {
                    Console.WriteLine($"    Operation type: {tx.operationType} - opening date: {tx.operationDate} - cash: {tx.cashSum}");
                }

                Console.WriteLine();
            }

        }

        public void RechargeOperations()
        {
            var operations =
                from tx in History
                where tx.OperationType == OperationType.InputCash
                join acc in Accounts on tx.AccountId equals acc.Id
                join user in Users on acc.UserId equals user.Id
                select new
                {
                    operationDate = tx.OperationDate,
                    cashSum = tx.CashSum,
                    owner = new
                    {
                        firstName = user.FirstName,
                        middleName = user.MiddleName,
                        surName = user.SurName
                    }
                };

            foreach (var op in operations)
            {
                Console.WriteLine($"Owner: {op.owner.firstName} {op.owner.middleName} {op.owner.surName} - opening date: {op.operationDate} - cash: {op.cashSum}");
            }
        }

        public void MoreThan(decimal sum)
        {
            var accaunts = Accounts
                .Where(x => x.CashAll > sum)
                .Join(Users,
                    x => x.UserId,
                    y => y.Id,
                    (x, y) => new
                    {
                        owner = new
                        {
                            firstName = y.FirstName,
                            middleName = y.MiddleName,
                            surName = y.SurName
                        },
                        openingDate = x.OpeningDate,
                        cashAll = x.CashAll
                    }
                )
                .GroupBy(x => x.owner)
                .Select(x => new
                { 
                    owner = x.Key,
                    accaunts = x.Select(x => x)
                });

            foreach (var item in accaunts)
            {
                Console.WriteLine($"Owner: {item.owner.firstName} {item.owner.middleName} {item.owner.surName}:");
                foreach (var acc in item.accaunts)
                {
                    Console.WriteLine($"    : Opening date: {acc.openingDate} - cash: {acc.cashAll}");
                }

                Console.WriteLine();
            }
        }
    }
}