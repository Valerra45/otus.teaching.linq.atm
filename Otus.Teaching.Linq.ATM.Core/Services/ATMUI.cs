﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Threading;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMUI
    {
        public ATMManager Manager { get; private set; }
        private User User { get; set; }
        private string[] Menu { get; set; }
        private int Arrow { get; set; }


        public ATMUI(ATMManager manager)
        {
            Manager = manager;

            Menu = new string[]
            {
                "User infomation",
                "User accounts",
                "Accounts history",
                "All recharge operations",
                "All users who have more than SUM on their account",
                "Exit"
            };
        }

        public void Run()
        {
            User = Authorization();

            if (User != null)
            {
                MainMenu();
            }
            else
            {
                Console.WriteLine("Login failed. Please try again.");
                Thread.Sleep(1000);
                Run();
            }
        }

        private User Authorization()
        {
            const int attemptsCount = 3;
            string login;
            string password;

            for (int i = 0; i < attemptsCount; i++)
            {
                Console.Clear();

                Console.Write("Login: ");
                login = Console.ReadLine();

                Console.Write("Password: ");
                password = Console.ReadLine();

                var user = Manager.SignIn(login, password);

                if (user != null)
                {
                    return user;
                }
            }

            return null;
        }

        private void UserInfomation()
        {
            Console.Clear();
            Console.WriteLine("User infomation:");
            Console.WriteLine();

            Console.WriteLine($"User: {User.FirstName} {User.MiddleName} {User.SurName}");
            Console.WriteLine($"Phone: {User.Phone}");
            Console.WriteLine($"Passport: {User.PassportSeriesAndNumber}");
            Console.WriteLine($"Registration: {User.RegistrationDate}");

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void UserAccounts(int id)
        {
            Console.Clear();
            Console.WriteLine("User accounts:");
            Console.WriteLine();

            Manager.UserAccounts(id);

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void UserHistory(int id)
        {
            Console.Clear();
            Console.WriteLine("User history:");
            Console.WriteLine();

            Manager.UserHistory(id);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void RechargeOperations()
        {
            Console.Clear();
            Console.WriteLine("All recharge operations:");
            Console.WriteLine();

            Manager.RechargeOperations();

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void MoreThan()
        {
            decimal sum;
            Console.Clear();
            Console.Write("Enter the amounts: ");

            sum = decimal.Parse(Console.ReadLine());

            Console.Clear();
            Console.WriteLine($"All users who have more than {sum} on their account:");
            Console.WriteLine();

            Manager.MoreThan(sum);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void MainMenu()
        {
            ConsoleKey key;

            while (true)
            {
                Console.Clear();
                Console.WriteLine($"Hi, { User.FirstName}!");
                Console.WriteLine();

                if (Arrow < (int)MenuActions.Infomation)
                {
                    Arrow = (int)MenuActions.Exit;
                }
                else if (Arrow > (int)MenuActions.Exit)
                {
                    Arrow = (int)MenuActions.Infomation;
                }

                for (int i = 0; i < Menu.Length; i++)
                {
                    Console.WriteLine($"{(Arrow == i ? ">>" : "  ")} {Menu[i]}");
                }

                key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    Arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    Arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((MenuActions)Arrow)
                    {
                        case MenuActions.Infomation:
                            UserInfomation();
                            break;
                        case MenuActions.Accounts:
                            UserAccounts(User.Id);
                            break;
                        case MenuActions.History:
                            UserHistory(User.Id);
                            break;
                        case MenuActions.Recharge:
                            RechargeOperations();
                            break;
                        case MenuActions.More:
                            MoreThan();
                            break;
                        case MenuActions.Exit:
                            Console.Clear();
                            return;
                    }
                }
            }
        }

        private enum MenuActions
        {
            Infomation,
            Accounts,
            History,
            Recharge,
            More,
            Exit
        }
    }
}
